package com.dmitry.photopicker.di

import android.content.Context
import com.dmitry.photopicker.model.Repository
import com.dmitry.photopicker.model.network.NetworkApi
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideRepository(context: Context, networkApi: NetworkApi): Repository {
        return Repository(context, networkApi)
    }
}