package com.dmitry.photopicker.di

import android.app.Application
import com.dmitry.photopicker.ui.detail.DetailFragment
import com.dmitry.photopicker.ui.photo.PhotoFragment
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, NetworkModule::class, RepositoryModule::class, ViewModelModule::class])
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(fragment: PhotoFragment)
    fun inject(fragment: DetailFragment)
}