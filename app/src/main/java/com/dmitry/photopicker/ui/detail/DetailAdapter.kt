package com.dmitry.photopicker.ui.detail

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.dmitry.photopicker.R
import com.dmitry.photopicker.model.network.response.Photos
import com.github.chrisbanes.photoview.PhotoView
import kotlinx.android.synthetic.main.recycler_detail_item.view.*
import java.lang.ref.WeakReference


class DetailAdapter(private var photoFragment: WeakReference<DetailFragment>) :
    PagedListAdapter<Photos, DetailAdapter.ViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recycler_detail_item,
                parent,
                false
            )
        )
        view.ivDetailPhoto.setOnClickListener {
            photoFragment.get()?.updateFullScreen()
        }
        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.also { photo ->
            Glide.with(holder.itemView.context)
                .load(photo.urls.regular)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.ivDetailPhoto)
        }
    }

    fun getPhotoByPosition(position: Int): Photos? {
        return getItem(position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var ivDetailPhoto: PhotoView = view.iv_detail_photo_item
    }

    companion object {
        internal val COMPARATOR = object : DiffUtil.ItemCallback<Photos>() {
            override fun areContentsTheSame(oldItem: Photos, newItem: Photos) = oldItem == newItem
            override fun areItemsTheSame(oldItem: Photos, newItem: Photos) = oldItem == newItem
        }
    }
}