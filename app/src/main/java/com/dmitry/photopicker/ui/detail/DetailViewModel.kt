package com.dmitry.photopicker.ui.detail

import android.app.WallpaperManager
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.net.Uri
import android.text.TextUtils
import android.util.DisplayMetrics
import android.util.Log
import android.view.WindowManager
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.dmitry.photopicker.R
import com.dmitry.photopicker.model.Repository
import com.dmitry.photopicker.model.network.response.Photos
import com.dmitry.photopicker.model.PagingDataSource.LoadPhotoDataSource
import com.dmitry.photopicker.model.PagingDataSource.LoadSearchPhotoDataSource
import com.dmitry.photopicker.utils.*
import kotlinx.coroutines.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import javax.inject.Inject


class DetailViewModel @Inject constructor(
    val repository: Repository
) : ViewModel() {

    private val TAG = "PhotoViewModel"
    val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
    lateinit var photos: LiveData<PagedList<Photos>>
    private lateinit var config: PagedList.Config
    var eventMessage = MutableLiveData<Event<String>>()
    val uriFile: MutableLiveData<Event<String>> = MutableLiveData()
    val progressBar: MutableLiveData<Int> = MutableLiveData()
    private var shareFilePath = ""

    fun initPagedList(searchText: String, pageToOpen: Int) {
        setProgressBar(loading)
        config = initConfig()
        photos = initializedPagedListBuilder(searchText, config, pageToOpen).build()
    }

    private fun initConfig(): PagedList.Config {
        return PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(loadSize)
            .setEnablePlaceholders(false)
            .build()
    }

    private fun initializedPagedListBuilder(
        searchText: String,
        config: PagedList.Config,
        pageToOpen: Int
    ): LivePagedListBuilder<Int, Photos> {

        val dataSourceFactory = object : DataSource.Factory<Int, Photos>() {
            override fun create(): DataSource<Int, Photos> {
                when (TextUtils.isEmpty(searchText)) {
                    true -> return LoadPhotoDataSource(
                        coroutineScope,
                        repository,
                        eventMessage,
                        pageToOpen
                    )
                    false -> return LoadSearchPhotoDataSource(
                        coroutineScope,
                        repository,
                        eventMessage,
                        searchText,
                        pageToOpen
                    )
                }
            }
        }
        return LivePagedListBuilder<Int, Photos>(dataSourceFactory, config)
    }

    private fun downloadFile(link: String, fileName: String): String {
        var path = "$dirPath/$fileName.jpg"
        var num = 0
        while (File(path).exists()) {
            path = dirPath + "/" + fileName + "(${++num}).jpg"
        }
        var downloadSuccess = false
        val url = URL(link)
        val `is` = url.openStream()
        val os = FileOutputStream(path)
        try {
            val b = ByteArray(2048)
            var length: Int
            length = `is`.read(b)
            while (length != -1) {
                os.write(b, 0, length)
                length = `is`.read(b)
            }
            downloadSuccess = true
        } catch (e: Exception) {
            eventMessage.postValue(Event(repository.context.getString(R.string.download_failed)))
        } finally {
            `is`.close()
            os.close()
            return if (downloadSuccess) path else ""
        }
    }

    fun setProgressBar(action: Int) {
        progressBar.postValue(action)
    }

    fun makeAction(link: String, fileName: String, action: Int) {
        coroutineScope.launch {
            setProgressBar(action)
            val downloadFilePath = downloadFile(link, fileName)
            if (TextUtils.isEmpty(downloadFilePath)) {
                setProgressBar(hideAction)
                return@launch
            }
            when (action) {
                saveAction -> {
                    notifyGalleryAboutFile(downloadFilePath)
                    val savedName =
                        downloadFilePath.substring(downloadFilePath.lastIndexOf("/") + 1)
                    setProgressBar(hideAction)
                    eventMessage.postValue(
                        Event(
                            String.format(
                                repository.context.getString(R.string.download_successful),
                                savedName
                            )
                        )
                    )
                }
                shareAction -> {
                    shareFilePath = downloadFilePath
                    uriFile.postValue(Event(shareFilePath))
                }
                setWallpaperAction -> {
                    notifyGalleryAboutFile(downloadFilePath)
                    createWallpaper(downloadFilePath)
                    setProgressBar(hideAction)
                }
            }
        }
    }

    private fun createWallpaper(path: String) {
        val displayMetrics = DisplayMetrics()
        val windowManager =
            repository.context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val heightScreen = displayMetrics.heightPixels
        val widthScreen = displayMetrics.widthPixels

        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(path, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, widthScreen, heightScreen)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        val decodedSampleBitmap = BitmapFactory.decodeFile(path, options)

        val wallpaperManager = WallpaperManager.getInstance(repository.context)
        try {
            wallpaperManager.setBitmap(decodedSampleBitmap)
        } catch (e: IOException) {
            Log.e(TAG, repository.context.getString(R.string.cannot_set_image_as_wallpaper), e)
            eventMessage.postValue(Event(repository.context.getString(R.string.cannot_set_image_as_wallpaper)))
        }
    }

    private fun calculateInSampleSize(
        options: BitmapFactory.Options, widthScreen: Int, heightScreen: Int
    ): Int {
        val heightImage = options.outHeight
        val widthImage = options.outWidth
        var inSampleSize = 1

        if (heightImage > heightScreen || widthImage > widthScreen) {
            val heightRatio = heightImage / heightScreen
            val widthRatio = widthImage / widthScreen
            inSampleSize = if (heightRatio < widthRatio) heightRatio else widthRatio
        }

        return inSampleSize
    }

    private fun notifyGalleryAboutFile(path: String) {
        repository.context.sendBroadcast(
            Intent(
                Intent.ACTION_MEDIA_SCANNER_SCAN_FILE,
                Uri.fromFile(File(path))
            )
        )
    }

    fun deleteAfterSharing() {
        delete(shareFilePath)
    }

    private fun delete(path: String) {
        coroutineScope.launch {
            if (!TextUtils.isEmpty(path)) {
                val file = File(path)
                if (file.exists()) {
                    file.delete()
                    shareFilePath = ""
                }
            }
            setProgressBar(hideAction)
        }
    }

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }
}