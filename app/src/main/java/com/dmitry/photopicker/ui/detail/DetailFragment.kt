package com.dmitry.photopicker.ui.detail

import android.Manifest
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.AbsListView
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dmitry.photopicker.BuildConfig
import com.dmitry.photopicker.PPApp
import com.dmitry.photopicker.R
import com.dmitry.photopicker.di.FactoryViewModel
import com.dmitry.photopicker.model.network.response.Photos
import com.dmitry.photopicker.utils.*
import com.dmitry.photopicker.utils.extentions.*
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.fragment_detail.*
import java.io.File
import java.lang.ref.WeakReference
import javax.inject.Inject


class DetailFragment : Fragment() {

    private lateinit var recyclerDetail: RecyclerView
    lateinit var detailViewModel: DetailViewModel
    lateinit var photoDetailAdapter: DetailAdapter
    lateinit var observer: RecyclerView.AdapterDataObserver
    var position = 0
    private var RESULT_SHARE_INTENT = 1001
    private val KEY_LAYOUT_DETAIL = "key_layout_detail"
    private val KEY_POSITION = "key_position"
    private val KEY_SEARCH_TEXT = "key_search_text"

    @Inject
    lateinit var viewModelFactory: FactoryViewModel

    companion object {
        val TAG = "DetailFragment"
        fun newInstance(position: Int, searchText: String) = DetailFragment().apply {
            arguments = Bundle().apply {
                putInt(KEY_POSITION, position)
                putString(KEY_SEARCH_TEXT, searchText)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as PPApp).appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        detailViewModel =
            ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
        if (savedInstanceState == null) {
            val loadPosition = arguments?.getInt(KEY_POSITION) ?: 0
            val searchText = arguments?.getString(KEY_SEARCH_TEXT) ?: ""
            val pageToOpen = loadPosition / loadSize + 1
            position = loadPosition % loadSize
            detailViewModel.initPagedList(searchText, pageToOpen)
        }
        initPhotoDetailAdapter()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_detail, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        detailViewModel.photos.observe(this, Observer {
            it?.let {
                photoDetailAdapter.submitList(it)
                if (!::recyclerDetail.isInitialized) {
                    initRecycler()
                }
                savedInstanceState?.let {
                    recyclerDetail.layoutManager?.onRestoreInstanceState(
                        savedInstanceState.getParcelable(KEY_LAYOUT_DETAIL)
                    )
                    position = savedInstanceState.getInt(KEY_POSITION)
                    updateToolbar()
                }
            }
        })
        detailViewModel.eventMessage.observe(this, Observer {
            it?.getContentIfNotHandled()?.let { message ->
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
        })
        detailViewModel.uriFile.observe(this, Observer {
            it?.getContentIfNotHandled()?.let { uri ->
                createShareIntent(uri)
            }
        })
        detailViewModel.progressBar.observe(this, Observer {
            it?.let { action ->
                updateProgressBar(action)
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar()
        setBottomActionsHeight()
        mainActivity().showSystemUI(false)
        bottom_save.setOnClickListener {
            runWithPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) {
                makeActionWithPhoto(saveAction)
            }
        }
        bottom_share.setOnClickListener {
            runWithPermissions(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ) {
                makeActionWithPhoto(shareAction)
            }
        }
    }

    private fun initToolbar() {
        toolbar_main.setNavigationIcon(R.drawable.ic_arrow_back_vector)
        toolbar_main.inflateMenu(R.menu.menu_detail)
        toolbar_main.setPadding(0, getStatusBarHeight(context!!), 0, 0)
        toolbar_main.setNavigationOnClickListener {
            mainActivity().onBackPressed()
        }
        toolbar_main.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.action_set_wallpaper -> {
                    makeActionWithPhoto(setWallpaperAction)
                    true
                }
                else -> super.onOptionsItemSelected(it)
            }
        }
    }

    private fun makeActionWithPhoto(action: Int) {
        updateCurrentPosition()
        detailViewModel.makeAction(getPhoto(position)!!.urls.raw, getPhoto(position)!!.id, action)
    }

    private fun updateCurrentPosition() {
        position =
            (recyclerDetail.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()
    }

    private fun getPhoto(position: Int): Photos? {
        return photoDetailAdapter.getPhotoByPosition(position)
    }

    private fun setBottomActionsHeight() {
        bottom_actions_wrapper.layoutParams.height =
            bottom_actions_wrapper.layoutParams.height + getNavigationBarHeight(context!!)
    }

    private fun createShareIntent(path: String) {
        val newUri = FileProvider.getUriForFile(
            context!!,
            BuildConfig.APPLICATION_ID + ".provider",
            File(path)
        )
        Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_STREAM, newUri)
            type = "image/jpeg"
            addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivityForResult(
                Intent.createChooser(this, getString(R.string.photo_by)),
                RESULT_SHARE_INTENT
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            RESULT_SHARE_INTENT -> {
                detailViewModel.deleteAfterSharing()
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_detail, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun initPhotoDetailAdapter() {
        photoDetailAdapter = DetailAdapter(WeakReference(this))
    }

    private fun initRecycler() {
        recyclerDetail = recycler_detail_photo
        recyclerDetail.apply {
            itemAnimator = null
            overScrollMode = View.OVER_SCROLL_NEVER
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            adapter = photoDetailAdapter
        }
        addSnapHelper()
        registerRecyclerObserver()
    }

    private fun addSnapHelper() {
        val helper = SnapHelperOneByOne()
        helper.attachToRecyclerView(recyclerDetail)
        recyclerDetail.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    maybeNotifySnapPositionChange(recyclerDetail)
                }
            }

            private fun maybeNotifySnapPositionChange(recyclerView: RecyclerView) {
                val centerView = helper.findSnapView(recyclerDetail.layoutManager)
                val newPossiblePosition =
                    centerView?.let { recyclerDetail.layoutManager?.getPosition(it) }
                if (newPossiblePosition != null) {
                    position = newPossiblePosition
                    updateToolbar()
                }
            }
        })
    }

    private fun registerRecyclerObserver() {
        observer = object : RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                recyclerDetail.scrollToPosition(position)
                photoDetailAdapter.unregisterAdapterDataObserver(observer)
                Handler().postDelayed({ detailViewModel.setProgressBar(hideAction) }, 300)
                updateToolbar()
            }
        }
        photoDetailAdapter.registerAdapterDataObserver(observer)
    }

    private fun updateToolbar() {
        tv_toolbar_title.text =getPhoto(position)?.user?.name
        tv_toolbar_title.setPaintFlags(tv_toolbar_title.paintFlags or Paint.UNDERLINE_TEXT_FLAG)
        tv_toolbar_title.setOnClickListener {
            val myWebLink = Intent(Intent.ACTION_VIEW)
            myWebLink.data = Uri.parse(getPhoto(position)?.links?.html)
            startActivity(myWebLink)
        }
        tv_toolbar_unsplash.setPaintFlags(tv_toolbar_title.paintFlags or Paint.UNDERLINE_TEXT_FLAG)
        tv_toolbar_unsplash.setOnClickListener {
            val myWebLink = Intent(Intent.ACTION_VIEW)
            myWebLink.data = Uri.parse(getString(R.string.unsplash_link))
            startActivity(myWebLink)
        }
    }

    fun updateFullScreen() {
        when (bottom_actions_wrapper.isVisible) {
            true -> {
                toolbar_main.beGone()
                bottom_actions_wrapper.beGone()
                mainActivity().hideSystemUI(false)
            }
            false -> {
                toolbar_main.beVisible()
                bottom_actions_wrapper.beVisible()
                mainActivity().showSystemUI(false)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(
            KEY_LAYOUT_DETAIL,
            recyclerDetail.layoutManager?.onSaveInstanceState()
        )
        updateCurrentPosition()
        outState.putInt(KEY_POSITION, position)
    }

    private fun updateProgressBar(action: Int) {
        when (action) {
            hideAction -> hideProgressBar()
            else -> showProgressBar(action)
        }
    }

    private fun hideProgressBar() {
        progress_layout_detail.beGone()
    }

    private fun showProgressBar(action: Int) {
        progress_bar_detail_text.text = getProgressBarText(context!!, action)
        progress_layout_detail.beVisible()
    }

    override fun onDestroy() {
        super.onDestroy()
        mainActivity().showSystemUI(false)
    }
}