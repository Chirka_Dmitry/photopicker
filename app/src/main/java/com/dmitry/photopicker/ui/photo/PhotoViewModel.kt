package com.dmitry.photopicker.ui.photo

import android.text.TextUtils
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.dmitry.photopicker.model.PagingDataSource.LoadPhotoDataSource
import com.dmitry.photopicker.model.PagingDataSource.LoadSearchPhotoDataSource
import com.dmitry.photopicker.model.Repository
import com.dmitry.photopicker.model.network.response.Photos
import com.dmitry.photopicker.utils.Event
import com.dmitry.photopicker.utils.loadSize
import com.dmitry.photopicker.utils.loading
import com.dmitry.photopicker.utils.pageSize
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import javax.inject.Inject


class PhotoViewModel @Inject constructor(
    val repository: Repository
) : ViewModel() {

    val TAG = "PhotoViewModel"
    val coroutineScope: CoroutineScope = CoroutineScope(Dispatchers.Default)
    lateinit var photos: LiveData<PagedList<Photos>>
    var eventMessage = MutableLiveData<Event<String>>()
    private val searchInput: MutableLiveData<String> = MutableLiveData()
    val progressBar: MutableLiveData<Int> = MutableLiveData()
    var mediatorLiveData: MediatorLiveData<PagedList<Photos>> = MediatorLiveData()
    private var config: PagedList.Config = initConfig()
    var searchText = ""

    init {
        searchPhotos("")
    }

    private fun initConfig(): PagedList.Config {
        return PagedList.Config.Builder()
            .setPageSize(pageSize)
            .setInitialLoadSizeHint(loadSize)
            .setEnablePlaceholders(false)
            .build()
    }

    fun searchPhotos(text: String) {
        setProgressBar(loading)
        searchText = text
        searchInput.postValue(searchText)
        photos = initializedPagedListBuilder(text, config).build()
        mediatorLiveData.addSource(photos) { list ->
            mediatorLiveData.value = list
        }
        /*photos = Transformations.switchMap(searchInput) {
            initializedPagedListBuilder(it, config).build()
        }*/
    }

    private fun initializedPagedListBuilder(searchText: String, config: PagedList.Config):
            LivePagedListBuilder<Int, Photos> {

        val dataSourceFactory = object : DataSource.Factory<Int, Photos>() {
            override fun create(): DataSource<Int, Photos> {
                when (TextUtils.isEmpty(searchText)) {
                    true -> return LoadPhotoDataSource(
                        coroutineScope,
                        repository,
                        eventMessage,
                        1
                    )
                    false -> return LoadSearchPhotoDataSource(
                        coroutineScope,
                        repository,
                        eventMessage,
                        searchText,
                        1
                    )
                }
            }
        }
        return LivePagedListBuilder<Int, Photos>(dataSourceFactory, config)
    }

    fun setProgressBar(action: Int) {
        progressBar.postValue(action)
    }

    @ExperimentalCoroutinesApi
    override fun onCleared() {
        super.onCleared()
        coroutineScope.cancel()
    }
}