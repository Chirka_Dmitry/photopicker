package com.dmitry.photopicker.ui.photo

import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.dmitry.photopicker.PPApp
import com.dmitry.photopicker.R
import com.dmitry.photopicker.di.FactoryViewModel
import com.dmitry.photopicker.model.network.response.Photos
import com.dmitry.photopicker.utils.extentions.*
import com.dmitry.photopicker.utils.getProgressBarText
import com.dmitry.photopicker.utils.getStatusBarHeight
import com.dmitry.photopicker.utils.hideAction
import kotlinx.android.synthetic.main.fragment_photos.*
import java.lang.ref.WeakReference
import javax.inject.Inject


class PhotoFragment : Fragment() {

    private lateinit var recyclerView: RecyclerView
    lateinit var viewModel: PhotoViewModel
    private lateinit var photoAdapter: PhotoAdapter
    var photos = ArrayList<Photos>()
    private var isFirstLaunch = true
    private var REQUEST_CODE = 1001
    private var KEY_LAYOUT = "key_layout"

    @Inject
    lateinit var viewModelFactory: FactoryViewModel

    companion object {
        val TAG = "PhotoFragment"
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        (context.applicationContext as PPApp).appComponent.inject(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel =
            ViewModelProviders.of(activity!!, viewModelFactory).get(PhotoViewModel::class.java)
        photoAdapter = PhotoAdapter(WeakReference(this))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.mediatorLiveData.observe(this, Observer {
            it?.let {
                photoAdapter.submitList(it)
                if (!::recyclerView.isInitialized) {
                    initPhotoAdapter()
                }
                if (savedInstanceState != null && isFirstLaunch) {
                    recyclerView.layoutManager?.onRestoreInstanceState(
                        savedInstanceState.getParcelable(KEY_LAYOUT)
                    )
                    isFirstLaunch = false
                }
                viewModel.setProgressBar(hideAction)
            }
        })
        viewModel.eventMessage.observe(this, Observer {
            it?.getContentIfNotHandled()?.let { message ->
                Toast.makeText(context, message, Toast.LENGTH_LONG).show()
            }
        })
        viewModel.progressBar.observe(this, Observer {
            it?.let { action ->
                updateProgressBar(action)
            }
        })
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_photos, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setSearchViewMargin()
        recycler_photo?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            var scrollingDown = false
            var scrollingUp = false

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                // Scrolling up
                if (dy > 0 && !scrollingUp) {
                    search_cardView.slideUp(200)
                    scrollingUp = true
                    scrollingDown = false

                }
                // Scrolling down
                if (dy <= 0 && !scrollingDown) {
                    search_cardView?.slideDown(200)
                    scrollingDown = true
                    scrollingUp = false
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING) {
                    search_cardView?.hideKeyboard()
                }
            }
        })
        btn_voice_search.setOnClickListener {
            val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
            startActivityForResult(intent, REQUEST_CODE)
        }
        btn_clear.setOnClickListener {
            if (TextUtils.isEmpty(et_search.text.toString())) {
                et_search.clearFocus()
                et_search.hideKeyboard()
            } else {
                et_search.setText("")
            }
        }
        et_search.setText(viewModel.searchText)
        et_search.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(newText: Editable?) {
                if (viewModel.searchText != newText.toString()) {
                    viewModel.searchPhotos(newText.toString())
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        })
        et_search.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                btn_voice_search.beGone()
                btn_clear.beVisible()
            } else {
                btn_voice_search.beVisible()
                btn_clear.beGone()
            }
        }
        super.onViewCreated(view, savedInstanceState)
    }

    private fun setSearchViewMargin() {
        search_cardView.addMarginTop(
            getStatusBarHeight(context!!) + resources.getDimension(R.dimen.search_top_margin).toInt()
        )
    }

    fun showDetailFragment(position: Int) {
        mainActivity().showDetailFragment(position, viewModel.searchText)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_CODE -> {
                if (resultCode == RESULT_OK && null != data) {
                    val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    et_search.setText(result?.get(0) ?: "")
                }
            }
        }
    }

    private fun initPhotoAdapter() {
        recyclerView = recycler_photo
        recyclerView.apply {
            setHasFixedSize(true)
            itemAnimator = null
            overScrollMode = View.OVER_SCROLL_NEVER
            layoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            adapter = photoAdapter
        }
    }

    private fun updateProgressBar(action: Int) {
        when (action) {
            hideAction -> hideProgressBar()
            else -> showProgressBar(action)
        }
    }

    private fun hideProgressBar() {
        progress_layout_photos.beGone()
    }

    private fun showProgressBar(action: Int) {
        progress_bar_photos_text.text = getProgressBarText(context!!, action)
        progress_layout_photos.beVisible()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(KEY_LAYOUT, recyclerView.layoutManager?.onSaveInstanceState())
    }
}