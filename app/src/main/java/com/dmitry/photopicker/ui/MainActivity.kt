package com.dmitry.photopicker.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dmitry.photopicker.R
import com.dmitry.photopicker.ui.detail.DetailFragment
import com.dmitry.photopicker.ui.photo.PhotoFragment


class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            showPhotoFragment()
        }
    }

    private fun showPhotoFragment() {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_layout, PhotoFragment(), PhotoFragment.TAG)
            .commit()
    }

    fun showDetailFragment(position: Int, searchText: String) {
        supportFragmentManager.beginTransaction()
            .add(
                R.id.fragment_layout,
                DetailFragment.newInstance(position, searchText),
                DetailFragment.TAG
            )
            .addToBackStack(null)
            .commit()
    }
}
