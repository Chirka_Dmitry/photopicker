package com.dmitry.photopicker.ui.photo

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.dmitry.photopicker.R
import com.dmitry.photopicker.model.network.response.Photos
import kotlinx.android.synthetic.main.recycler_photo_item.view.*
import java.lang.ref.WeakReference


class PhotoAdapter(private var photoFragment: WeakReference<PhotoFragment>) :
    PagedListAdapter<Photos, PhotoAdapter.ViewHolder>(COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.recycler_photo_item,
                parent,
                false
            )
        )
        view.ivPhoto.setOnClickListener {
            photoFragment.get()?.showDetailFragment(view.adapterPosition)
        }
        return view
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.also { photo ->
            holder.ivPhoto.aspectRatio = photo.height.toDouble() / photo.width.toDouble()
            holder.ivPhoto.setBackgroundColor(Color.parseColor(photo.color))
            Glide.with(holder.itemView.context)
                .load(photo.urls.small)
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(holder.ivPhoto)
        }
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var ivPhoto: AspectRatioImageView = view.iv_photo_item
    }

    companion object {
        internal val COMPARATOR = object : DiffUtil.ItemCallback<Photos>() {
            override fun areContentsTheSame(oldItem: Photos, newItem: Photos) = oldItem == newItem
            override fun areItemsTheSame(oldItem: Photos, newItem: Photos) = oldItem == newItem
        }
    }
}