package com.dmitry.photopicker

import android.app.Application
import com.dmitry.photopicker.di.AppComponent
import com.dmitry.photopicker.di.DaggerAppComponent

class PPApp : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent
            .builder()
            .application(this)
            .build()
    }
}