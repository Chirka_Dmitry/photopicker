package com.dmitry.photopicker.utils

import android.os.Environment

val baseUrl = "https://api.unsplash.com/"

val accessKey = "16f0278288e87d42b354e05f9bcbdad8873b9aaaba8cd13f961458856bac2224"
val secretKey = "38ae93c429f696b7d5fcee076eb46b0ad5189a5c4b63d3be6c1fba58e7208b5c"
val unsplashAppName = "PhotoPicker"
val isLoggingEnabled = true

val dirPath =
    Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString()

var pageSize = 30
var loadSize = 30

var loading = 0
var saveAction = 1
var shareAction = 2
var setWallpaperAction = 3
var hideAction = 4
