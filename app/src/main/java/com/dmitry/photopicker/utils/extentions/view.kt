package com.dmitry.photopicker.utils.extentions

import android.animation.ObjectAnimator
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.view.inputmethod.InputMethodManager

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
    this.clearFocus()
}

fun View.addMarginTop(margin: Int) {
    val param = layoutParams as ViewGroup.MarginLayoutParams
    param.topMargin = margin
    layoutParams = param
}

fun View.slideUp(duration: Int) {
    ObjectAnimator.ofFloat(this, "translationY",
        -height.toFloat() - (layoutParams as ViewGroup.MarginLayoutParams).let { it.topMargin })
        .apply {
            this.duration = duration.toLong()
            this.interpolator = LinearInterpolator()
        }.start()
}

fun View.slideDown(duration: Int) {
    ObjectAnimator.ofFloat(this, "translationY", 0F).apply {
        this.duration = duration.toLong()
        this.interpolator = LinearInterpolator()
    }.start()
}

fun View.beInvisibleIf(beInvisible: Boolean) = if (beInvisible) beInvisible() else beVisible()

fun View.beVisibleIf(beVisible: Boolean) = if (beVisible) beVisible() else beGone()

fun View.beGoneIf(beGone: Boolean) = beVisibleIf(!beGone)

fun View.beInvisible() {
    visibility = View.INVISIBLE
}

fun View.beVisible() {
    visibility = View.VISIBLE
}

fun View.beGone() {
    visibility = View.GONE
}

fun View.isVisible() = visibility == View.VISIBLE

fun View.isInvisible() = visibility == View.INVISIBLE

fun View.isGone() = visibility == View.GONE