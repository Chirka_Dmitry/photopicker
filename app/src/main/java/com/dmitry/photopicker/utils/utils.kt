package com.dmitry.photopicker.utils

import android.content.Context
import com.dmitry.photopicker.R

fun getStatusBarHeight(context: Context): Int {
    var statusBarHeight = 0
    val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
    if (resourceId > 0) {
        statusBarHeight = context.resources.getDimensionPixelSize(resourceId)
    }
    return statusBarHeight
}

fun getNavigationBarHeight(context: Context): Int {
    val resourceId = context.resources.getIdentifier("navigation_bar_height", "dimen", "android")
    return if (resourceId > 0) {
        context.resources.getDimensionPixelSize(resourceId)
    } else 0
}

fun getProgressBarText(context: Context, action: Int): String {
    return when (action) {
        saveAction -> context.resources.getString(R.string.downloading_file)
        shareAction -> context.resources.getString(R.string.sharing_file)
        setWallpaperAction -> context.resources.getString(R.string.setting_as_wallpaper)
        else -> context.resources.getString(R.string.loading)
    }
}