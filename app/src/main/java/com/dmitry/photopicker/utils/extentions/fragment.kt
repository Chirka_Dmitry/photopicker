package com.dmitry.photopicker.utils.extentions

import androidx.fragment.app.Fragment
import com.dmitry.photopicker.ui.MainActivity

fun Fragment.mainActivity(): MainActivity {
    return activity as MainActivity
}