package com.dmitry.photopicker.model.network.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CurrentUserCollection(
    val cover_photo: List<String>,
    val curated: Boolean,
    val id: Int,
    val published_at: String,
    val title: String,
    val updated_at: String,
    val user: List<String>
) : Parcelable