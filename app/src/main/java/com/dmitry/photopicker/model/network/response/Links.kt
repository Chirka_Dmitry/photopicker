package com.dmitry.photopicker.model.network.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Links(
    val download: String,
    val download_location: String,
    val html: String,
    val self: String
) : Parcelable