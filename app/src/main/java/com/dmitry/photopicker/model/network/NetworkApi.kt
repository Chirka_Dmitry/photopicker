package com.dmitry.photopicker.model.network

import com.dmitry.photopicker.model.network.response.Photos
import com.dmitry.photopicker.model.network.response.SearchPhotos
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface NetworkApi {

    @GET("/photos")
    suspend fun getPhotos(
        @Query("client_id") clientId: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): Response<List<Photos>>

    @GET("/search/photos")
    suspend fun getSearchPhotos(
        @Query("client_id") clientId: String,
        @Query("query") query: String,
        @Query("page") page: Int,
        @Query("per_page") pageSize: Int
    ): Response<SearchPhotos>
}