package com.dmitry.photopicker.model

import android.content.Context
import com.dmitry.photopicker.model.network.NetworkApi
import com.dmitry.photopicker.model.network.response.Photos
import com.dmitry.photopicker.model.network.response.SearchPhotos
import retrofit2.Response
import javax.inject.Inject

class Repository @Inject constructor(val context: Context, private val networkApi: NetworkApi) {
    val TAG = "Repository"

    suspend fun getPhotos(accessKey: String, page: Int, pageSize: Int): Response<List<Photos>> {
        return networkApi.getPhotos(accessKey, page, pageSize)
    }

    suspend fun getSearchPhotos(
        accessKey: String, query: String, page: Int, pageSize: Int
    ): Response<SearchPhotos> {
        return networkApi.getSearchPhotos(accessKey, query, page, pageSize)
    }
}