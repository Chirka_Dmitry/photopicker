package com.dmitry.photopicker.model.PagingDataSource

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import com.dmitry.photopicker.R
import com.dmitry.photopicker.model.Repository
import com.dmitry.photopicker.model.network.response.Photos
import com.dmitry.photopicker.utils.Event
import com.dmitry.photopicker.utils.accessKey
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch

class LoadPhotoDataSource(
    private val scope: CoroutineScope,
    private val repository: Repository,
    private var loadPhotoError: MutableLiveData<Event<String>>,
    private var pageKey: Int
) : PageKeyedDataSource<Int, Photos>() {

    private var lastPage: Int? = null

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Photos>
    ) {
        scope.launch {
            try {
                val response = repository.getPhotos(accessKey, pageKey, params.requestedLoadSize)
                if (response.isSuccessful) {
                    lastPage =
                        response.headers()["x-total"]?.toInt()?.div(params.requestedLoadSize)
                    callback.onResult(
                        response.body()!!, if (pageKey > 1) pageKey - 1 else null, pageKey + 1
                    )
                } else {
                    showError(repository.context.resources.getString(R.string.error_from_server))
                }
            } catch (exception: Exception) {
                showError(repository.context.resources.getString(R.string.error_fetch_data))
            }
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Photos>) {
        scope.launch {
            try {
                val response = repository.getPhotos(accessKey, params.key, params.requestedLoadSize)
                if (response.isSuccessful) {
                    val nextPage = if (params.key == lastPage) null else params.key + 1
                    callback.onResult(response.body()!!, nextPage)
                } else {
                    showError(repository.context.resources.getString(R.string.error_from_server))
                }
            } catch (exception: Exception) {
                showError(repository.context.resources.getString(R.string.error_fetch_data))
            }
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Photos>) {
        scope.launch {
            try {
                val response = repository.getPhotos(accessKey, params.key, params.requestedLoadSize)
                if (response.isSuccessful) {
                    val prevPage = if (params.key == 1) null else params.key - 1
                    callback.onResult(response.body()!!, prevPage)
                } else {
                    showError(repository.context.resources.getString(R.string.error_from_server))
                }
            } catch (exception: Exception) {
                showError(repository.context.resources.getString(R.string.error_fetch_data))
            }
        }
    }

    private fun showError(errorMessage: String) {
        loadPhotoError.postValue(Event(errorMessage))
        Log.e("LoadPhotoDataSource", errorMessage)
    }

    @ExperimentalCoroutinesApi
    override fun invalidate() {
        super.invalidate()
        scope.cancel()
    }
}