package com.dmitry.photopicker.model.network.response

data class SearchPhotos(
    val results: List<Photos>,
    val total: Int,
    val total_pages: Int
)